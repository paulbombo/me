# A guide to Paul Bombo

## Motivation for this document

Twiga Tech is a distributed team, making it a bit more challenging to get to know me.
For my team, this is a user guide to me and how I work, how I interpret my role, my _interesting_ personality quirks and some bits in between.
For others within Twiga, this document should help answer, `Who is Paul Bombo, What does he do? and Why does he have such great jokes?` and offer a reference for when I inevitably do something that surprises.

## So, who is Paul?

The summary on my CV reads...

> _Seasoned software engineer with proven experience in conceptualizing and delivering from the ground up, complex and scalable enterprise software systems using a variety of proprietary and open-source technologies._

I have been a builder for hire since 2009 and over the years I have done so for a variety of companies in industries ranging from financial services, communication, logistics and insurance.

I'd describe myself as a curious person and have broad interests in everything from [distributed systems](https://dl.acm.org/doi/pdf/10.1145/3080202), [complexity in software](https://www.youtube.com/watch?v=rI8tNMsozo0&t=9s), to [semiotics](https://en.wikipedia.org/wiki/Semiotics), [user experience](https://www.interaction-design.org/literature/article/the-kano-model-a-tool-to-prioritize-the-users-wants-and-desires) and [people](https://www.marcprensky.com/writing/Prensky%20-%20Digital%20Natives,%20Digital%20Immigrants%20-%20Part1.pdf).

My [Myers-Briggs](https://en.wikipedia.org/wiki/Myers%E2%80%93Briggs_Type_Indicator) personality type is [INTJ](https://www.16personalities.com/intj-personality), the only bit that may surprise is the degree to which I am introverted.

## My role

My role is to provide the team an environment to do good work and produce great outcomes.
I see myself as being responsible for providing an environment that produces consistent success of whatever team I am on.

### For the team I lead this means...

#### Providing

- A compelling direction - Clear objectives and meaningful work
- An enabling structure - Process and cadence for checkpoints, planning, review (retros and recaps) and feedback
- Context for work we do - Ensuring the team has enough information, the right skills, resources, and rewards
- Coaching - Guidance on individual/work questions and challenges being experienced

#### Monitoring

- Product and team health [Feature usage metrics, SLOs, Product roadmap and OKR delivery, Employee NPS]
- Productivity metrics [Deployment frequency, Wall-clock cycle time, Change fail rates]

#### Ensuring

- That projects we take on have more than one kind of [time horizon](https://en.wikipedia.org/wiki/Time_horizon)
- That what we are working on the right problem, right solution - "build the right thing and build the thing right".
- That we are working well together

### For Twiga Tech...

- Improving the quality of engineering decisions made, especially those with long-lasting/far-reaching consequences
- Developing and maintaining engineering excellence across our technology architecture, the solutions we develop, our approach to work and, collaboration and, planning _measure twice, cut once_.

If I am doing well in my role, the team doesn't depend on me to get on with things and there is value in the work it does.

## What I value

- Evidence based approaches to work. A clear _why_ to the _what_ helps eliminate most ambiguity. If you have insufficient evidence (the why) to support your position/argument/idea/work (the what), that's sounds like an experiment and I value those too
- Experimenting, these are great for learning and if they consume a significant amount of yours or the team's time (10% of whatever unit of time you pick) should be documented
- [Outcome driven work](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done) which comes from a clear understanding of team goals. You should always be able to clearly demonstrate how you and/or your work are contributing to team goals.
- Documentation, I strive for and encourage everyone to document _everything_ and though I do have a strong preference for text, other formats are fine as well.
- Structure because [_"constraints breed creativity"_](https://observer.com/2016/10/how-constraints-breed-creativity/)
- My time, please don't waste it

I also have a map of weighted qualities I value in team members

|                 |                                                                                                           |            |
|-----------------|-----------------------------------------------------------------------------------------------------------|:------------:|
| Quality         | Description                                                                                               | Importance |
| Competence      | Has the technical expertise and experience to do the job effectively                                      | 20         |
| Trustworthiness | Can be relied upon to be straight with you and to follow through on commitments                           | 15         |
| Energy          | Brings the right attitude to the job \(isn’t burned\-out or disengaged\)                                  | 20         |
| People Skills   | Gets along well with others on the team and supports collaboration                                        | 15         |
| Focus           | Sets priorities and sticks to them, instead of veering off in all directions                              | 10         |
| Judgement       | Exercises good sense, especially under pressure or when faced with making sacrifices for the greater good | 20         |
|                 |                                                                                                           |            |

## My expectations

1. You were hired to add value because of the experience and skill you have. Because of this assumption, I assume that you can structure yourself/work and that you will communicate consistently, not just when I ask.

2. If you can't attend a meeting, let me know in advance, not 15 minutes into the call, any apology that begins with _"Sorry, my previous meeting spilled over..."_ is just extremely disingenuous.

3. Video on for recurring team meetings as well as any other meeting that is under an hour and not ad-hoc. A lot of nuance is lost in an audio only meeting.

4. Ask questions if you need anything clarified. If you are on my team, I will always have 5 mins to spare for you, regardless of how packed my day is or how busy I may seem. If you need 5 minutes of my time, just send me a Slack/Teams message asking, “Hey Paul, do you have 5 minutes to talk about X?”

5. Done means whatever it is you were working on is in production and is in use. This generally means that you will need to followup past just completing the work. For instance, if you have written a feature but it’s not shipped, then it’s not done. If the code is in production, but the feature is broken or not working as expected, it’s not done either.

## How I work

I see things as [systems](https://smile.amazon.com/gp/product/1603580557/ref=x_gr_w_bb_sout?ie=UTF8&tag=x_gr_w_bb_sout-20&linkCode=as2&camp=1789&creative=9325&creativeASIN=1603580557&SubscriptionId=1MGPYB6YW3HWK55XCGG2). I reduce all complex things (including humans) into systems (process, behaviour and motivations).
I take great joy in attempting to understand [how different systems and their parts interact](https://mitpress.mit.edu/books/turtles-termites-and-traffic-jams). 
When I see large or small inefficiencies in systems, I’d like to fix them with your help.

I tend to have high expectations and will challenge you on what you are doing. This doesn’t necessarily mean that you are doing a poor job. It often means that I believe you can progress even more.

### Communication

You can contact me 24/7 - I'll always respond as soon as I can.

In order of preference, you can reach me on:

1. **Slack/Teams**

    I have Slack/Teams installed everywhere (even on my personal mobile phone) meaning it's the quickest way to get my attention. I like it because it lets me respond in my own without being interrupted.
    <br />
    <br />

2. **Email**

    Email me if you want in depth answers or have complicated questions that chat doesn't feel quite right for.
    <br />
    <br />

3. **Phone calls**

    They're good for time-sensitive conversations, getting my attention, or talking through something personal/complex.
    If I haven't saved your number, I probably won't answer your call. If I don't, please slack/text me so I know that it's you.
    <br />

If you want more structured time to meet, send me a calendar invite. If you can't find time on it, you can `ping` me using either of the above.

### Work hours

My work hours are 9am to 5pm on weekdays. I sometimes work on the weekends and after hours. If you see me working outside of these hours, it is because I have chosen to. I do not expect that you are going to work on the weekend. I might Slack you things, but unless the thing says URGENT, it can always wait until work begins for you on Monday.

## One-on-Ones

One on one meetings are your meetings. I can't stress this enough. I won't use these for status checks, we have other meetings and async tools to do this.
The 1:1 is for you and I to build a good relationship. Sometimes there will be things we need to talk about, sometimes there won’t be, we should still meet, regardless. When the 1:1 feels over, and there is remaining time I always have a couple of things too discuss. This is brainstorming, and the issues are usually front-of-mind topics that probably affect your work.

Here is what you can expect from it:

- It will be at least 30 minutes, every two weeks
- A third of the time is for you - Ask me anything, we can discuss anything that you feel is important to you.
- A third of the time is for me
- A third of the time is for the long term - Career growth, how you feel about your work and performance, the team, the org.

It will be on Teams because:

- It maintains a log of any links or notes we share.
- It is async and you can add notes about your topics you want to discuss ahead of time

## Feedback

Feedback is central to building trust and respect in a team.
I value feedback through it we reinforce behaviors we want to see repeated and that spread and discourage behaviors we'd like to eliminate. Disagreement is feedback and the sooner we learn how to efficiently disagree with each other, the sooner we’ll trust and respect each other more. Ideas don’t get better with agreement.

### With me
  - If you have a problem with the way I am doing something, let me know. Especially if you feel like I am making a mistake. Don't let it fester, ping me as soon as you can on Slack or Teams.
  - Positive feedback is something I enjoy. You can deliver it publicly if you like but privately is also fine.
### With you
  - When something goes wrong, I'll let you know in private as soon as possible. I take opportunities for negative feedback as a chance to put out embers before they become fires.
  - Positive feedback will be shared publicly mostly during standups and retros.

## Personality Quirks

- It takes a lot for me to get annoyed but you will know when I am, I'll be sharp and acerbic. This happens if you try and be too clever/cute with it i.e. _"ukijaribu kunibeba ufala"_.

- It irks me when people blindly adopt (tools, ideas, etc) rather than try to understand why and then adapting. It is almost guaranteed that your context is never the same as the source you are copying from.

- I am direct and often blunt; it is how I get people to do better. While it is appreciated by some, I know that it can be a bit too much for some people. If this a problem, please let me know and I’ll adapt.

- I can be obsessed with improving things, whilst there is obviously a lot of value in what has come before us — we stand on the shoulders of those who have come before us etc etc — I do believe it is imperative that we keep querying the what, how and why of our work.

- I can overpower others in meetings. Especially because I like to talk and more so when the discussion is going off track. It is something I am conscious of.

- I hold some _strange_ beliefs like developers should be able to deploy into production even on a Friday evening. If you are worried that you might break something, that is a symptom of a larger problem — our systems are brittle.

## Some quotes I like

"Anybody can dig a hole and plant a tree. But make sure it survives. You have to nurture it, you have to water it, you have to keep at it until it becomes rooted so it can take care of itself. There are so many enemies of trees."

— Wangari Maathai

<br />
“If we have data, let’s look at data. If all we have are opinions, let’s go with mine.”

— James L. Barksdale, former CEO of Netscape

<br />
“Success is most often achieved by those who don't know that failure is inevitable.”

— Coco Chanel, Believing in Ourselves: The Wisdom of Women

<br />
“You read everything—that’s part of the job,” he said. “You accumulate all this trivia, and you hope that someday maybe a millionth of it will be useful.”

― Walter Isaacson in The Innovators: How a Group of Inventors, Hackers, Geniuses, and Geeks Created the Digital Revolution

<br />
"For what is important when we give children a theorem to use is not that they should memorize it. What matters most is that by growing up with a few very powerful theorems one comes to appreciate how certain ideas can be used as tools to think with over a lifetime. One learns to enjoy and to respect the power of powerful ideas. **One learns that the most powerful idea of all is the idea of powerful ideas.**"

― Seymour Papert

<br />
“People in life arrive quite early at a very good level, and don’t move too much because they don’t push themselves enough”

― Arsene Wenger
